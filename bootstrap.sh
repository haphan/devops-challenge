#!/usr/bin/env bash
#####
# This file is intended to bootstrap an Ubuntu VM with minimum dependencies via
# cloud-init or remote-exec provisioner in terraform.
# This file use keep as small as possible and shall use interfaces defined by
# Makefile
#####
apt-add-repository ppa:ansible/ansible -y
apt update -y
apt install -y git ansible make
